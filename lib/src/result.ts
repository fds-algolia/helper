import { SINGLE_RESULT_TYPE } from './constants';

export interface ResultStats {
  hasResults: boolean;
  results: Array<Result>;
  hitCount: number;
  pageCount: number;
  page: number;
  hitsPerPage: number;
  isLastPage: boolean;
  refinedType: Result | null;
}

export class Result {
  private readonly _type: string;
  private readonly _items: any[];
  private _active: boolean;
  private _count: number;

  constructor(type: string, items: any[] = []) {
    this._type = type;
    this._items = items;
    this._active = type === SINGLE_RESULT_TYPE;
    this._count = 0;
  }
  get active(): boolean {
    return this._active;
  }
  set active(active: boolean) {
    this._active = active;
  }
  get type(): string {
    return this._type;
  }
  get items(): any[] {
    return this._items;
  }
  get count(): number {
    return this._count;
  }
  set count(count: number) {
    this._count = count;
  }
  public updateItems(items: any[], clear: boolean = false) {
    if (clear) {
      this.clearItems();
    }
    for (const item of items) {
      this.items.push(item);
    }
  }
  public clearItems() {
    this.items.length = 0;
  }
}
