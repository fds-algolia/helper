import algoliasearchHelper, { AlgoliaSearchHelper, SearchParameters, SearchResults } from 'algoliasearch-helper';
import { fromEventPattern, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { algolia } from './client';
import { ResultEvent } from './events';
import { ResultStats, Result } from './result';
import { SortBy,
  FacetValue,
  FacetList,
  FacetListConfig,
  FacetSearchHit, FacetDefinition,
} from './facets';
import { SINGLE_RESULT_TYPE } from './constants';

export class Search {
  /**
   * Observable returning latest results
   */
  public resultObserver!: Observable<ResultStats|null>;
  /**
   * The AlgoliaSearchHelper reference for manipulating the specified index
   */
  protected helper: AlgoliaSearchHelper;
  /**
   * Stats from latest result set
   */
  protected stats: ResultStats = {
    hasResults: false,
    hitCount: 0,
    pageCount: 0,
    hitsPerPage: 16,
    page: 1,
    isLastPage: false,
    results: [],
    refinedType: null,
  };
  /**
   * The internal array of all facets with their current state
   */
  private _facets: FacetList[] = [];
  /**
   * The currently active facets
   */
  private _activeFacets: FacetDefinition = {};
  /**
   * The internal array of filtered search results
   */
  private _results: any[] = [];
  /**
   * Refinement Configs for facets
   */
  private readonly facetLists: FacetList[];
  private readonly facetAttributes: string[];

  constructor(index: string, refinements: FacetListConfig) {
    this.helper = algoliasearchHelper(algolia, index, {
      disjunctiveFacets: refinements.attributes,
    });
    this.facetLists = refinements.lists;
    this.facetAttributes = refinements.attributes;
    // Set up the search
    this.helper.searchOnce({ disjunctiveFacets: refinements.attributes, analytics: false })
      .then(resp => this.initSearch(resp.content, resp.state));
    this.initResultObserver();
  }

  /**
   * The searches distinct property
   */
  get distinct(): number {
    return 0;
  }
  /**
   * Getter for internal property _facets
   */
  get facets(): FacetList[] {
    return this._facets;
  }
  /**
   * Getter for internal property _activeFacets
   */
  get activeFacets(): FacetDefinition {
    return this._activeFacets;
  }
  set activeFacets(facets: FacetDefinition) {
    this._activeFacets = facets;
  }
  get hasActiveFacets(): boolean {
    return Object.keys(this._activeFacets).length > 0;
  }
  /**
   * Getter for internal property _results
   */
  get results(): Result[] {
    return this._results;
  }
  /**
   * The result object for the currently selected type
   */
  get activeResult(): Result | null {
    const aRes = this.results.find(res => res.active);
    if (!aRes) {
      return null;
    }
    return aRes;
  }
  /**
   * Getter for current Algolia search state
   */
  get state(): SearchParameters {
    return this.helper.state;
  }

  /**
   * Main search function.
   * check for query and add analytics = false if no query
   */
  public search(): void {
    this.setSearchAnalytics();
    this.helper
      .setQueryParameter('distinct', this.distinct)
      .search();
  }
  /**
   * Clears all refinements or just one attribute
   *
   */
  public clearRefinements(attribute?: string): void {
    this.helper.clearRefinements(attribute);
  }
  /**
   * Toggles facets
   */
  public toggleFacet(attribute: string, value: string, refined: boolean): void {
    if (!refined) {
      this.addFacets({ [attribute]: [value] });
    } else {
      this.removeFacets({ [attribute]: [value] });
    }
  }
  /**
   * adds facets to search instance and the active facets object
   *
   */
  public addFacets(facets: FacetDefinition): void {
    for (const facet in facets) {
      if (facets.hasOwnProperty(facet)) {
        this.activeFacets[facet] = facets[facet];
        this.helper.addDisjunctiveFacetRefinement(facet, facets[facet]);
      }
    }
  }
  /**
   * Removes facets from search instance and the active facets object
   *
   */
  public removeFacets(facets: FacetDefinition): void {
    for (const facet in facets) {
      if (facets.hasOwnProperty(facet)) {
        delete this.activeFacets[facet];
        this.helper.removeDisjunctiveFacetRefinement(facet, facets[facet]);
      }
    }
  }
  /**
   * Reset a facets values after being refined
   */
  public resetFacet(attribute: string) {
    const refinedFacet = this.facets.find(f => f.attribute === attribute);
    if (refinedFacet) {
      this.helper.searchOnce({ disjunctiveFacets: this.facetAttributes, analytics: false })
        .then(resp => {
          refinedFacet.values = resp.content.getFacetValues(attribute, { sortBy: [SortBy.COUNT_DES] }) as FacetValue[];
        });
    }
  }
  /**
   *
   * Search within available facets
   *
   */
  public async searchFacets(attribute: string, query: string, maxHits: number = 99, userState = {}) {
    const refinedFacet = this.facets.find(f => f.attribute === attribute);
    const refinedVals: any = await this.helper.searchForFacetValues(attribute, query, maxHits, userState);
    let hasResults = false;

    if (refinedFacet) {
      if (refinedVals.facetHits.length) {
        hasResults = true;
        refinedFacet.values = refinedVals.facetHits.map(
          (val: FacetSearchHit) => {
            return {
              name: val.highlighted,
              count: val.count,
              isRefined: val.isRefined,
              isExcluded: false,
            };
          },
        );
      } else {
        refinedFacet.values = [];
      }
    }
    return { hasQuery: query.length > 0, hasResults };
  }
  /**
   * Set the query in the search helper
   * Optionally trigger search
   *
   */
  public setQuery(q: string) {
    this.helper.setQuery(q);
  }
  /**
   * Clears the search query and optionally searches
   *
   */
  public clearQuery(search: boolean = false) {
    this.setQuery('');
    this.clearRefinements();
    this.activeFacets = {};
  }
  /**
   * Loads the next page of results
   */
  public nextPage(): void {
    this.setPage(this.helper.getCurrentPage() + 1);
  }
  /**
   * Loads the previous page of results
   */
  public prevPage(): void {
    this.setPage(this.helper.getCurrentPage() - 1);
  }
  /**
   * Move to a specific page of results
   */
  public setPage(page: number): void {
    this.setSearchAnalytics();
    this.helper
      .setQueryParameter('distinct', this.distinct)
      .setPage(page)
      .search();
  }

  public setQueryParams(params: { [param: string]: string|number|boolean }) {
    const props = Object.getOwnPropertyNames(params);
    for (const prop of props) {
      // @ts-ignore
      this.helper.setQueryParameter(prop, props[prop]);
    }
  }

  protected setSearchAnalytics() {
    const q = this.helper.getQuery().query;
    const hasQuery = q && q.length > 0;
    this.helper.setQueryParameter('analytics', hasQuery);
  }
  /**
   * Initialize search
   *
   */
  protected initSearch(results: SearchResults, state: SearchParameters): void {
    if (this.facetLists.length) {
      this.initFacets(results);
    }
    this.results.push(new Result(SINGLE_RESULT_TYPE));
  }
  /**
   * Initialize facets
   *
   */
  protected initFacets(sr: SearchResults): void {
    for (const facet of this.facetLists) {
      const values = sr.getFacetValues(facet.attribute, { sortBy: facet.sortBy }) as FacetValue[];
      facet.values = values;
      facet.canShowMore = values.length > facet.limit;
      this.facets.push(facet);
    }
  }
  /**
   * Initialize result observer
   *
   */
  protected initResultObserver() {
    // Create the main results observable
    this.resultObserver = fromEventPattern<ResultEvent>(
      (h) => this.helper.on('result', h),
      (h) => this.helper.off('result', h),
    ).pipe(map((result: ResultEvent) => this.onResult(result.results, result.state)));
  }
  /**
   * new result hook
   *
   */
  protected onResult(result: SearchResults, params: SearchParameters): ResultStats | null {
    for (const res of this.results) {
      res.clearItems();
    }
    this.updateFacetVals(result);
    this.updateResults(result, [ SINGLE_RESULT_TYPE ]);
    this.updateStats(result);
    return this.stats;
  }
  /**
   * Updates facets
   *
   */
  protected updateFacetVals(sr: SearchResults): void {
    for (const facet of this.facets) {
      facet.values = sr.getFacetValues(facet.attribute, { sortBy: facet.sortBy }) as FacetValue[];
      facet.canShowMore = facet.values.length > facet.limit;
    }
  }
  /**
   * Updates Results
   *
   */
  protected updateResults(result: SearchResults, types: string[]): void {
    let currentType;
    let currentHits: any[];
    for (const type of types) {
      currentType = this.results.find(res => res.type === type);
      if (currentType) {
        if (types.length === 1) {
          currentType.updateItems(result.hits, true);
          currentType.count = result.nbHits;
        } else {
          const facetVals = result.getFacetValues('type', {}) as FacetValue[];
          const selectedVal = facetVals.find(v => v.name === type);
          if (selectedVal) {
            currentHits = result.hits.filter(h => h.type === type);
            currentType.count = currentHits.length ? selectedVal.count : 0;
          }
        }
      }
    }
  }
  /**
   * Updates stats
   *
   */
  protected updateStats(result: SearchResults): void {
    this.stats = {
      hasResults: result.nbHits > 0,
      results: this.results[0] ? this.results[0].items : [],
      hitCount: result.nbHits,
      pageCount: result.nbPages,
      page: result.page + 1,
      isLastPage: result.nbPages <= result.page + 1,
      hitsPerPage: result.hitsPerPage,
      refinedType: null,
    };
  }
}
