import { SearchParameters, SearchResults } from 'algoliasearch-helper';

export interface SearchEvent {
  state: SearchParameters;
  results: SearchResults;
}
export interface ChangeEvent extends SearchEvent{
  isPageReset: boolean;
}
export interface SearchForFacetValuesEvent {
  state: SearchParameters;
  facet: string;
  query: string;
}
export interface SearchOnceEvent {
  state: SearchParameters;
}
export interface ResultEvent {
  results: SearchResults;
  state: SearchParameters;
}
export interface ErrorEvent {
  error: Error;
}
