export interface FacetValue {
  name: string;
  count: number;
  isRefined: boolean;
  isExcluded: boolean;
}

export interface FacetDefinition {
  [facet: string]: string[];
}

export interface FacetListConfig {
  attributes: string[];
  lists: FacetList[];
}

export interface FacetSearchHit {
  value: string;
  highlighted: string;
  count: number;
  isRefined: boolean;
}

export interface FacetSearchResult {
  facetHits: FacetSearchHit;
  processingTimeMS: number;
}

export interface FacetList {
  id: string;
  active: boolean;
  attribute: string;
  label: string;
  values: FacetValue[];
  sortBy: SortBy[];
  limit: number;
  operator?: RefinementOperator;
  canShowMore?: boolean;
  showingMore?: boolean;
  placeHolder?: string;
  toggleShowMore(cb?: () => void): void;
  searchable?: boolean;
}

export enum SortBy {
  COUNT_ASC = 'count:asc',
  COUNT_DES = 'count:desc',
  NAME_ASC = 'name:asc',
  NAME_DES = 'name:desc',
  REF = 'isRefined',
}

export enum RefinementOperator {
  OR = 'or',
  AND = 'and',
}
