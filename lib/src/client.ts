

export const algolia: Client = algoliasearch(
  process.env.VUE_APP_ALGOLIA_APP_ID as string,
  process.env.VUE_APP_ALGOLIA_SEARCH_KEY as string,
);
